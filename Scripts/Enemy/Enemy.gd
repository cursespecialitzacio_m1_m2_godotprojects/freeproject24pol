extends CharacterBody2D

class_name Enemy

@onready var initialPosition : Vector2 = global_position

# region Enum
enum EnemyState { FOLLOW, IDDLE, ATTACK, HIT }
# endregion


# region Constants

########### Animations ###########
const IDDLE_DISTANCE : float = 0.5

########### Stats ###########
const SPEED : float = 100
const COOLDOWN_ATTACK : float = 2.0
const MAX_HEALTH : float = 9.0
const MAX_DAMAGE : float = 1.5
const MIN_DAMAGE : float = 0.5

# endregion

# region Variables
var gravity = ProjectSettings.get_setting("physics/2d/default_gravity")
var playerTarget : CharacterBody2D
var currentState : EnemyState = EnemyState.IDDLE
var direction = 0
var attackCoroutine = false
var currentScale = 0
var currentHealth = 0
# endregion

func _ready():
	playerTarget = null
	changeState(EnemyState.IDDLE)
	currentScale=$CollisionShape2D/Area2D.scale.x
	currentHealth=MAX_HEALTH
	var listOfCheckPoints = get_tree().get_nodes_in_group("Checkpoint")
	for i in listOfCheckPoints:
		var check := i as Checkpoint
		check.playerRest.connect(initial_stats)
	
	var listOfPlayers = get_tree().get_nodes_in_group("Player")
	for i in listOfPlayers:
		var check := i as PlayerBody
		check.playerDie.connect(initial_stats)
	

func _physics_process(delta):
	if(!notDeath()): 
		return
	
	gravity_process(delta)
	updateState()
	setDirection()
	move_and_slide()

#################### Physics ####################

func gravity_process(delta : float):
	if not is_on_floor():
		velocity.y += gravity * delta

func setDirection():
	if direction:
		velocity.x = direction * SPEED
	else:
		velocity.x = move_toward(velocity.x, 0, SPEED)

#################### EventFunctions ####################

# region Events

func onWeaponCollision(body_rid, body, body_shape_index, local_shape_index):
	if(body.is_in_group("Player")):
		var player := body as PlayerBody
		player.applyDamage(3.0) 

func onAttackRangeExit(body_rid, body, body_shape_index, local_shape_index):
	changeState(EnemyState.FOLLOW)

func onFollowRangeEntered(body_rid, body, body_shape_index, local_shape_index):
	if(body.is_in_group("Player")):
		playerTarget=body
		changeState(EnemyState.FOLLOW)

func onFollowRangeExit(body_rid, body, body_shape_index, local_shape_index):
	changeState(EnemyState.IDDLE)		

func onAnimationFinished():
	if($AnimatedSprite2D.animation=="ATTACK"):
		$AnimatedSprite2D.animation="IDLE"
	elif($AnimatedSprite2D.animation=="HIT"):
		if(checkThereIsAPlayer($AttackArea.get_overlapping_bodies())):
			changeState(EnemyState.ATTACK)
		elif(checkThereIsAPlayer($FollowArea.get_overlapping_bodies())):
			changeState(EnemyState.FOLLOW)
		else:
			changeState(EnemyState.IDDLE)

func onAnimationChanged():
	if($AnimatedSprite2D.animation=="ATTACK"):
		$AnimationPlayer.play("ATTACK")

func onAttackRangeEnter(body_rid, body, body_shape_index, local_shape_index):
	if(body.is_in_group("Player")):
		playerTarget=body
		changeState(EnemyState.ATTACK)

# endregion

################################# StateMachine #################################

# region ChangeState
func changeState(newState : EnemyState):
	if(currentState==EnemyState.HIT && !notDeath()):
		return
	exitState()
	startState(newState)
	currentState=newState

func exitState():
	match currentState:
		EnemyState.IDDLE:
			pass
		EnemyState.ATTACK:
			attackCoroutine=false
		EnemyState.FOLLOW:
			direction=0
		EnemyState.HIT:
			pass

func startState(newState : EnemyState):
	match newState:
		EnemyState.IDDLE:
			$AnimatedSprite2D.animation="IDLE"
			playerTarget=null
		EnemyState.ATTACK:
			attackCoroutineExec()
		EnemyState.FOLLOW:
			$AnimatedSprite2D.animation="RUN"
		EnemyState.HIT:
			$AnimatedSprite2D.animation="HIT"

# endregion

# region UpdateStates

func updateState():
	match currentState:
		EnemyState.IDDLE:
			updateIDDLE()
		EnemyState.ATTACK:
			updateATTACK()
		EnemyState.FOLLOW:
			updateFOLLOW()
		EnemyState.HIT:
			updateHIT()

func updateIDDLE():
	$AnimatedSprite2D.play()

func updateATTACK():
	$AnimatedSprite2D.play()
	var diff = playerTarget.global_position-global_position
	flipSprite(diff)

func updateFOLLOW():
	$AnimatedSprite2D.play()
	if(playerTarget==null): return;
	var diff = playerTarget.global_position-global_position
	var abs = abs(diff.x)
	if(abs <= IDDLE_DISTANCE && $AnimatedSprite2D.animation!="IDLE"):
		$AnimatedSprite2D.animation="IDLE"
	elif(abs > IDDLE_DISTANCE && $AnimatedSprite2D.animation=="IDLE"):
		$AnimatedSprite2D.animation="RUN"
	flipSprite(diff)

func updateHIT():
	$AnimatedSprite2D.play()

# endregion

################################# Coroutines #################################

func attackCoroutineExec():
	$AnimatedSprite2D.animation="IDLE"
	attackCoroutine=true
	while (attackCoroutine):
		await get_tree().create_timer(COOLDOWN_ATTACK).timeout
		if(attackCoroutine):
			if($AnimatedSprite2D.animation!="HIT"): $AnimatedSprite2D.animation="ATTACK"

################################ Others ####################################

func flipSprite(diff : Vector2):
	diff = diff.normalized()
	direction = diff.x
	$AnimatedSprite2D.flip_h = direction<0
	if(direction<0):
		$CollisionShape2D/Area2D.scale.x=-(currentScale)
	else:
		$CollisionShape2D/Area2D.scale.x=currentScale

func checkThereIsAPlayer(list : Array[Node2D]):
	for x in list:
		if(x.is_in_group("Player")):
			return true
	return false

func applyDamage(damage : float) -> float :
	currentHealth-=damage
	if(!notDeath()):
		die();
		return 100;
	else:
		return 0;

func notDeath():
	return currentHealth>0;

func revive():
	currentHealth = MAX_HEALTH;
	$AnimatedSprite2D.visible=true;

func die():
	$AnimatedSprite2D.visible = false;
	currentHealth=-1;
	attackCoroutine=false;


func save():
	return {
		"filename" : get_scene_file_path(),
		"parent" : get_parent().get_path(),
		"position_x":initialPosition.x,
		"position_y":initialPosition.y,
		"currentHealth":currentHealth,
	}

func load(object):
	var x := object["position_x"] as float
	var y := object["position_y"] as float
	if(object!=null && x==initialPosition.x && y==initialPosition.y):
		currentHealth = object["currentHealth"]
		position.x = object["position_x"]
		position.y = object["position_y"]


func initial_stats():
	revive()
	changeState(EnemyState.IDDLE)
	global_position = initialPosition

