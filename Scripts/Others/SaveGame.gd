extends Node
class_name SaveGameHandler
func _ready():
	load_game()


func _process(delta):
	if(Input.is_action_just_pressed("SaveGame")):
		SaveGameProcess()

func SaveGameProcess():
	var save_game = FileAccess.open("user://savegame.save", FileAccess.WRITE)
	var save_nodes = get_tree().get_nodes_in_group("Persist")
	for node in save_nodes:
		if node.scene_file_path.is_empty():
			print("persistent node '%s' is not an instanced scene, skipped" % node.name)
			continue

		if !node.has_method("save"):
			print("persistent node '%s' is missing a save() function, skipped" % node.name)
			continue

		var node_data = node.call("save")

		var json_string = JSON.stringify(node_data)
		
		save_game.store_line(json_string)

	save_game.flush()
	save_game.close()

func load_game():
	var save_nodes = get_tree().get_nodes_in_group("Persist")
	if not FileAccess.file_exists("user://savegame.save"):
		for i in save_nodes:
			i.call("initial_stats") 
		return 
 

	var save_game = FileAccess.open("user://savegame.save", FileAccess.READ)
	while save_game.get_position() < save_game.get_length():
		var json_string = save_game.get_line()

		var json = JSON.new()

		var parse_result = json.parse(json_string)
		if not parse_result == OK:
			print("JSON Parse Error: ", json.get_error_message(), " in ", json_string, " at line ", json.get_error_line())
			continue

		var node_data = json.get_data()
		for i in save_nodes:
			i.call("load", node_data)



