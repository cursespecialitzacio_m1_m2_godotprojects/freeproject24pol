extends CharacterBody2D

class_name Checkpoint
signal playerRest()

var saveGame : bool = false;
var GRAVITY = ProjectSettings.get_setting("physics/2d/default_gravity")

func _ready():
	saveGame=false  

func _process(delta):
	velocity.y += GRAVITY * delta
	if(Input.is_action_just_pressed("Interact") && saveGame):
		Rest();
	move_and_slide()
	$AnimatedSprite2D.play()

func Rest():
	playerRest.emit();
	var objectPlayer = get_tree().get_first_node_in_group("Player");
	var player := objectPlayer as PlayerBody
	player.player_current_health = player.PLAYER_MAX_HEALTH * player.player_level;
	player.lastCheckPoint = global_position;
	var saveGameH := get_parent().get_node("SaveGameHandler") as SaveGameHandler
	saveGameH.SaveGameProcess()

func onTriggerEnter(body_rid:RID, body:Node2D, body_shape_index:int, local_shape_index:int):
	if(body.is_in_group("Player")):
		$RichTextLabel.visible=true;
		saveGame=true;


func onTriggerExit(body_rid:RID, body:Node2D, body_shape_index:int, local_shape_index:int):
	if(body.is_in_group("Player")):
		$RichTextLabel.visible=false;
		saveGame=false;
