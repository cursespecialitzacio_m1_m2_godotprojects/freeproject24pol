extends Node2D

class_name PlayerRoot


func _ready():
	pass 


func _process(delta):
	pass

func save():
	var saveObject = {
		"position_x": $PlayerBody.initialPosition.x,
		"position_y": $PlayerBody.initialPosition.y,
		"filename" : get_scene_file_path(),
		"parent" : get_parent().get_path(),
		"player_current_health":$PlayerBody.player_current_health,
		"player_level":$PlayerBody.player_level,
		"current_player_max_damage":$PlayerBody.current_player_max_damage,
		"current_player_min_damage":$PlayerBody.current_player_min_damage,
		"cposition_x":$PlayerBody.global_position.x,
		"cposition_y":$PlayerBody.global_position.y,
		"checkPoint_x":$PlayerBody.lastCheckPoint.x,
		"checkPoint_y":$PlayerBody.lastCheckPoint.y,
		"currentExperience":$PlayerBody.currentExperience,
		"maxExperience":$PlayerBody.maxExperience
	}
	print("Saving Player: ", saveObject)
	return saveObject

func load(object):
	var x := object["position_x"] as float
	var y := object["position_y"] as float
	if(object!=null && x == $PlayerBody.initialPosition.x && y == $PlayerBody.initialPosition.y):
		print("Loading Object: ",object)
		$PlayerBody.player_current_health = object["player_current_health"]
		$PlayerBody.current_player_max_damage = object["current_player_max_damage"]
		$PlayerBody.current_player_min_damage = object["current_player_min_damage"]
		$PlayerBody.player_current_health = object["player_current_health"]
		$PlayerCamera.global_position.x = object["cposition_x"]
		$PlayerCamera.global_position.y = object["cposition_y"]
		$PlayerBody.global_position.x = object["cposition_x"]
		$PlayerBody.global_position.y = object["cposition_y"]
		$PlayerBody.lastCheckPoint.x = object["checkPoint_x"]
		$PlayerBody.lastCheckPoint.y = object["checkPoint_y"]
		$PlayerBody.currentExperience = object["currentExperience"]
		$PlayerBody.maxExperience = object["maxExperience"]

func initial_stats():
	$PlayerBody.player_current_health=$PlayerBody.PLAYER_MAX_HEALTH * $PlayerBody.player_level;
	$PlayerBody.current_player_max_damage=$PlayerBody.PLAYER_MAX_DAMAGE_BASE * $PlayerBody.player_level;
	$PlayerBody.current_player_min_damage=$PlayerBody.PLAYER_MIN_DAMAGE_BASE * $PlayerBody.player_level;
	$PlayerBody.lastCheckPoint = $PlayerBody.initialPosition;
	$PlayerBody.currentExperience = 0;
	$PlayerBody.maxExperience = 300;
	var saveObject = {
		"position_x": $PlayerBody.initialPosition.x,
		"position_y": $PlayerBody.initialPosition.y,
		"filename" : get_scene_file_path(),
		"parent" : get_parent().get_path(),
		"player_current_health":$PlayerBody.player_current_health,
		"player_level":$PlayerBody.player_level,
		"current_player_max_damage":$PlayerBody.current_player_max_damage,
		"current_player_min_damage":$PlayerBody.current_player_min_damage,
		"cposition_x":$PlayerBody.global_position.x,
		"cposition_y":$PlayerBody.global_position.y,
		"checkPoint_x":$PlayerBody.lastCheckPoint.x,
		"checkPoint_y":$PlayerBody.lastCheckPoint.y,
		"currentExperience":$PlayerBody.currentExperience,
		"maxExperience":$PlayerBody.maxExperience
	}
	print("Initial Stats: ", saveObject)