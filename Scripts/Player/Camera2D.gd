extends Camera2D
class_name CameraPlayer

@export var playerTarget : CharacterBody2D;


func _ready():
	if playerTarget == null:
		playerTarget = get_parent().get_node("PlayerBody");



func _process(delta):
	if(playerTarget == null): return;
	var current_pos = global_position
	var target_pos = playerTarget.global_position
	var new_pos = current_pos.lerp(target_pos, 0.05)
	global_position = new_pos

