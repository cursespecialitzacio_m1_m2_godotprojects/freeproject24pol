extends CharacterBody2D

class_name PlayerBody

signal playerDie

@onready var initialPosition : Vector2 = global_position;

var GRAVITY = ProjectSettings.get_setting("physics/2d/default_gravity")
enum PlayerState { RUN, IDDLE, JUMP, FALL, ATTACK1, ATTACK2, ATTACK3, HIT }
enum Combo { ATTACK1, ATTACK2, ATTACK3, NONE }

@export var speed = 100;
@export var currentStatePlayerInput : PlayerState = PlayerState.IDDLE

var flipped = false;
var direction : Vector2;
var currentAttackState : Combo = Combo.NONE
var old_animation : String = ""


# region PlayerVariables
@export var player_level : int = 1
var PLAYER_MAX_HEALTH : float = 20;
var player_current_health : float;
var PLAYER_MAX_DAMAGE_BASE : float = 10.0;
var current_player_max_damage : float;
var PLAYER_MIN_DAMAGE_BASE : float = 6.0;
var current_player_min_damage : float; 
var lastCheckPoint : Vector2;
var currentExperience : float;
var EXPERIENCE_NEEDED_PER_LEVEL = 300.0
var maxExperience : float;
#endregion


# region Ready & Process 
################################# Ready / Process #################################

func _ready():
	old_animation="IDDLE"
	direction = Vector2.ZERO
	$CollisionShape2D/Area2D/CollisionShape2D.disabled=true
	currentStatePlayerInput = PlayerState.IDDLE	

func _physics_process(delta):
	PhysicsControl(delta) # Physics process like velocity in horizontal and vertical

func _process(delta):
	direction = Vector2.ZERO # The player's movement vector.
	InputControl() # Input process
	StateMachine() # Change the state Machine of the player
	AnimationControl() # Animation Setting

# endregion

# region Process Principal Functions
################################# Process Functions #################################

func InputControl():
	
	if !notDeath() || !notHitting():
		return

	if Input.is_action_just_pressed("attack"):
		if(notAttacking()):
			currentStatePlayerInput=PlayerState.ATTACK1
			$CollisionShape2D/Area2D/AnimationPlayer.play("attack1")
		else:
			if(currentStatePlayerInput==PlayerState.ATTACK3):
				currentAttackState=Combo.ATTACK1
			elif(currentStatePlayerInput==PlayerState.ATTACK1):
				currentAttackState=Combo.ATTACK2
			elif currentStatePlayerInput==PlayerState.ATTACK2:
				currentAttackState=Combo.ATTACK3
	
	if(notAttacking()):
		if Input.is_action_pressed("move_right"):
			direction.x += 1
		if Input.is_action_pressed("move_left"):
			direction.x -= 1

func StateMachine():

	if(!notHitting()): return;

	if(notAttacking()):
		if direction.x != 0 || velocity.y != 0:
			direction = direction.normalized() * speed
			if(velocity.y>0):
				currentStatePlayerInput=PlayerState.FALL
			elif(velocity.y<0):
				currentStatePlayerInput=PlayerState.JUMP
			elif(direction.x!=0):
				currentStatePlayerInput=PlayerState.RUN
		else:
			currentStatePlayerInput=PlayerState.IDDLE

func PhysicsControl(delta):
	
	velocity.x = direction.x * delta * speed
	
	if(is_on_floor()):
		velocity.y = 0
	else:
		velocity.y += GRAVITY * delta
		
	if Input.is_action_just_pressed("Jump") && is_on_floor() && notAttacking() && notHitting():
		velocity.y = -500
		
	move_and_slide()

func AnimationControl():
	
	$AnimatedSprite2D.animation = PlayerState.keys()[currentStatePlayerInput]
	
	if( direction.x > 0 ): 
		flipped = false;
		$CollisionShape2D/Area2D.scale.x = 1
	elif( direction.x < 0 ): 
		flipped = true;
		$CollisionShape2D/Area2D.scale.x = -1
		
	$AnimatedSprite2D.flip_h = flipped;
	$AnimatedSprite2D.play()
# endregion

# region Other Functions
################################# Other Functions #################################

func notAttacking():
	return currentStatePlayerInput!=PlayerState.ATTACK1 && currentStatePlayerInput!=PlayerState.ATTACK2 && currentStatePlayerInput!=PlayerState.ATTACK3

func notDeath():
	return player_current_health>0;

func notHitting():
	return currentStatePlayerInput!=PlayerState.HIT

func _on_animated_sprite_2d_animation_changed():
	var new_name = $AnimatedSprite2D.animation
	if(old_animation=="ATTACK1" && new_name=="IDDLE" && currentAttackState==Combo.ATTACK2):
		currentStatePlayerInput=PlayerState.ATTACK2
		currentAttackState=Combo.NONE
		$CollisionShape2D/Area2D/AnimationPlayer.stop()
		$CollisionShape2D/Area2D/AnimationPlayer.clear_queue()
		$CollisionShape2D/Area2D/AnimationPlayer.clear_caches()
		$CollisionShape2D/Area2D/AnimationPlayer.play("attack2")
	elif(old_animation=="ATTACK2" && new_name=="IDDLE" && currentAttackState==Combo.ATTACK3):
		currentStatePlayerInput=PlayerState.ATTACK3
		currentAttackState=Combo.NONE
		$CollisionShape2D/Area2D/AnimationPlayer.stop()
		$CollisionShape2D/Area2D/AnimationPlayer.clear_queue()
		$CollisionShape2D/Area2D/AnimationPlayer.clear_caches()
		$CollisionShape2D/Area2D/AnimationPlayer.play("attack3")
	elif(old_animation=="ATTACK3" && new_name=="IDDLE" && currentAttackState==Combo.ATTACK1):
		currentStatePlayerInput=PlayerState.ATTACK1
		currentAttackState=Combo.NONE
		$CollisionShape2D/Area2D/AnimationPlayer.stop()
		$CollisionShape2D/Area2D/AnimationPlayer.clear_queue()
		$CollisionShape2D/Area2D/AnimationPlayer.clear_caches()
		$CollisionShape2D/Area2D/AnimationPlayer.play("attack1")
	elif(new_name=="HIT"):
		currentAttackState=Combo.NONE
	elif(new_name=="IDDLE"):
		$CollisionShape2D/Area2D/AnimationPlayer.stop()
		$CollisionShape2D/Area2D/AnimationPlayer.clear_queue()
		$CollisionShape2D/Area2D/AnimationPlayer.clear_caches()
		currentStatePlayerInput=PlayerState.IDDLE
		currentAttackState=Combo.NONE
	old_animation=new_name


func onTriggerEnter(body_rid, body, body_shape_index, local_shape_index):
	if body.is_in_group("Enemy"):
		var enemy := body as Enemy
		var totalDamage = randf_range(current_player_min_damage, current_player_max_damage)
		if(currentStatePlayerInput==PlayerState.ATTACK1):
			currentExperience+=enemy.applyDamage(totalDamage*0.33)
		elif(currentStatePlayerInput==PlayerState.ATTACK1):
			currentExperience+=enemy.applyDamage(totalDamage*0.66)
		elif(currentStatePlayerInput==PlayerState.ATTACK1):
			currentExperience+=enemy.applyDamage(totalDamage)
	checkLevelUp();

func checkLevelUp():
	print("CHECK LEVEL UP")
	if(currentExperience>=maxExperience):
		print("ENTRA")
		player_level+=1;
		maxExperience=EXPERIENCE_NEEDED_PER_LEVEL*player_level;
		current_player_max_damage=PLAYER_MAX_DAMAGE_BASE*player_level
		current_player_min_damage = PLAYER_MIN_DAMAGE_BASE*player_level

func applyDamage(damage : float):
	player_current_health-=damage
	currentStatePlayerInput=PlayerState.HIT
	if(!notDeath()):
		global_position = lastCheckPoint
		player_current_health = PLAYER_MAX_HEALTH * player_level
		playerDie.emit()
		var saveGameH := get_parent().get_parent().get_node("SaveGameHandler") as SaveGameHandler
		saveGameH.SaveGameProcess()


func _on_animated_sprite_2d_animation_finished():
	if($AnimatedSprite2D.animation=="HIT"):
		currentStatePlayerInput=PlayerState.IDDLE
		currentAttackState=Combo.NONE
		
# endregion
