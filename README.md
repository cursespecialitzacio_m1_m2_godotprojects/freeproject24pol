#   FreeProject Godot 2024 Pol Gonzalo
##  Autor
<b>Pol Gonzalo Méndez</b>

---
## Millores respecte al Platform

- [x] Guardar Partida
- [x] Sistema experiencia i nivell
- [x] 3 tipos d'enemics més.
- [ ] sistema missions (no está fet però si agafa experiencia amb la mort del enemic)

---
## Controls
* (WASD) Moure
* (L-Click) Atacar
* (Space) Saltar
* (E) Interactuar
* (F5) Guardar -> TE UN BUG QUE GUARDA ALS ENEMICS COM MORTS PERÒ NO M'HA DONAT TEMPS A TERMINAR-HO